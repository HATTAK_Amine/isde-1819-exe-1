def split_data(X, y, tr_fraction=0.5):
    """Split the data X,y into two random subsets."""
    raise NotImplementedError


def count_digits(y):
    """Count the number of elements in each class."""
    raise NotImplementedError


def fit(Xtr, ytr):
    """Compute the average centroid for each class."""
    raise NotImplementedError
