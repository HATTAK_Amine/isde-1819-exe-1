import numpy as np
import math
from fun_utils import split_data, count_digits, fit

X = np.random.randint(0, 255, size=(11, 784))
y = np.random.randint(0, 10, size=(11, ))


# TEST split_data(X, y, tr_fraction=0.5)
def test_split_data(Xtr, ytr, Xts, yts, tr_frac, n_samples, n_feats):

    assert type(Xtr) == np.ndarray
    assert type(ytr) == np.ndarray
    assert type(Xts) == np.ndarray
    assert type(yts) == np.ndarray

    assert Xtr.ndim == 2
    assert ytr.ndim == 1
    assert Xtr.ndim == 2
    assert yts.ndim == 1

    tr_size = math.floor(n_samples * tr_frac)
    ts_size = n_samples - tr_size

    assert Xtr.shape[0] == tr_size
    assert ytr.size == tr_size
    assert Xts.shape[0] == ts_size
    assert yts.size == ts_size

    assert Xtr.shape[1] == n_feats
    assert Xts.shape[1] == n_feats


print "Testing `split_data`..."

x_tr, y_tr, x_ts, y_ts = split_data(X, y)
test_split_data(x_tr, y_tr, x_ts, y_ts, 0.5, X.shape[0], X.shape[1])

# TEST split_data(X, y, tr_fraction=0.7)
x_tr, y_tr, x_ts, y_ts = split_data(X, y, 0.7)
test_split_data(x_tr, y_tr, x_ts, y_ts, 0.7, X.shape[0], X.shape[1])


# TEST count_digits(y)
def test_count_digits(res, s, counts):

    assert type(res) == np.ndarray
    assert res.dtype == int

    assert res.size == s

    for a_idx, a in enumerate(counts):
        assert res[a_idx] == a


print "Testing `count_digits`..."

c = count_digits(np.array([0, 0, 0]))
test_count_digits(c, 1, [3])
c = count_digits(np.array([1, 3, 5]))
test_count_digits(c, 3, [1, 1, 1])
c = count_digits(np.array([1, 1, 3, 3]))
test_count_digits(c, 2, [2, 2])
c = count_digits(np.array([1, 1, 2, 2, 3]))
test_count_digits(c, 3, [2, 2, 1])


# TEST fit(Xtr, ytr)
def test_fit(res, n_c, n_feats):

    assert type(res) == np.ndarray

    assert res.ndim == 2
    assert res.shape[0] == n_c
    assert res.shape[1] == n_feats


print "Testing `fit`..."

c = fit(X, y)
test_fit(c, np.unique(y).size, X.shape[1])

print "All tests successful!"
